
__precompile__(true)

module Headcount

using Plots
using CSV
using DataFrames
using Gtk

export main

function main(save_fig=false; img_ext=".png")
    file_path, file_name, file_ext = get_file_info()
    headcount_dataframe = get_dataframe(file_path)
    headcount_data = clean_headcount_dataframe(headcount_dataframe)
    headcount_plot = plot_headcount_data(headcount_data, file_name)

    if (save_fig)
      savefig(headcount_plot * img_ext)
    end

    return headcount_plot
end

function get_file_info()
    file_path = open_dialog("Select headcount file")
    file_split = split(split(file_path, "\\")[end], ".")
    file_name = file_split[1]
    file_ext = file_split[2]

    return [file_path, file_name, file_ext]
end

function get_dataframe(file_path::String)::DataFrame
    csv_file = CSV.File(file_path, header=1)
    df = DataFrame(csv_file)

    return df
end

function clean_headcount_dataframe(df::DataFrame)::Array{Int64,1}
    data = Int64[]

    for row in 1:size(df,1)
        for col in 1:size(row,1)
            for val in Array(df[row, 2:size(df, 2)])
                if isa(val, Number)
                    push!(data, val)
                elseif !isa(val, Missing) && isa(tryparse(Int, val), Number)
                    push!(data, tryparse(Int, val))
                else
                    push!(data, 0)
                end
            end
        end
    end

    return data
end

function plot_headcount_data(data::Array{Int64,1}, file_name)
    xticks_spacing = 0:9:length(data)
    xticks_label = [Integer((x / 9) + 1) for x in xticks_spacing]

    headcount_plot = plot(data,
         title=file_name,
         xlabel="Day",
         ylabel="Headcount",
         legend=false,
         xticks=(xticks_spacing, xticks_label))

    return headcount_plot
end

end # module Headcount
